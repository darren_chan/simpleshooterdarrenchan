There are 10 problems with this game. Can you fix these problems and write down your solutions?


1. There is no music when game is started
On BackgroundMusic check play on awake and loop

2. x does not start the game

In Manager Script enter the following in update

if(Input.GetKey("x"))
	GameStart();

3. Enemies are not getting killed by the players bullets

added a circle collider 2D to the Player Bullet prefab

4. Player's ship can go out of the visible screen area
add 4 box colliders around the scence in background


5. Enemy bullets will not kill the player ship
player script change 	if( layerName == "Bullet (Enemies)" || layerName == "Enemy") to 	if( layerName == "Bullet (Enemy)" || layerName == "Enemy")


6. Make the bullets from the player's ship come out faster
In the PlayerBullet prefab, change bullet script speed to 20

7. The first enemy wave is supped to be 3 ships, but the left one is missing
using the Wave prefab add enemy to it.

8. When an enemy ship is killed, the player should get 100 points not 1
In Enemy script change

Manager.current.AddPoint(hp); to Manager.current.AddPoint(point);

9. High score is not showing or updating
In Mangager Script add

	highScoreGUIText.text = highScoreKey + highScore.ToString();

10. Make a creative addition to the game!

When space bar is pressed, destroy all enimes ships ( no points rewarded)


====================

Source code is at https://bitbucket.org/yusufpisan/simpleshooter/


You need to create a "Pull Request" in BitBucket.
See https://confluence.atlassian.com/bitbucket/tutorial-learn-about-pull-requests-in-bitbucket-cloud-774243385.html if necessary.


About Forking
      When you work with another user's public Bitbucket repository, typically you have read access to the code but not write access. This is where the concept of forking comes in. Here's how it works:
      Fork the repository to copy it to your own account.
      Clone the forked repository from Bitbucket to your local system.
      Make changes to the local repository.
      Push the changes to your forked repository on Bitbucket.
      Create a pull request from the original repository you forked to add the changes you made.
      Wait for the repository owner to accept or reject your changes.


When using version control with Unity

You need to make the Meta files visible in Unity
    * Edit >Project Settings > Editor -> Version Control    Visible Meta Files 

Add the .gitignore file
    * http://kleber-swf.com/the-definitive-gitignore-for-unity-projects/ Depending on your operating system, the .gitignore file may not be visible in the Explorer, Finder, etc but you can see it at the DOS and Unix shells.

Make sure your repository is readable by 'yusufpisan'

This project already has visible meta files and a .gitignore file (as well as a visible-gitignore.txt to remind you to always add .gitignore